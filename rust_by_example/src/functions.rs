mod associated_functions_and_methods;
mod closures;
mod closures_capturing;
mod functions;

pub fn main() {
    functions::main();
    associated_functions_and_methods::main();
    closures::main();
    closures_capturing::main();
}
